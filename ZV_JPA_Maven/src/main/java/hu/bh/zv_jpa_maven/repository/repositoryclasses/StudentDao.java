package hu.bh.zv_jpa_maven.repository.repositoryclasses;

import hu.bh.zv_jpa_maven.repository.CRUDRepository;
import hu.bh.zv_jpa_maven.repository.entity.Student;
import lombok.Data;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Singleton
@Data
public class StudentDao implements CRUDRepository<Student, String> {

    @PersistenceContext
    private EntityManager em;


    @Override
    public void delete(Student entity) {
        em.remove(entity);
    }

    @Override
    public void deleteAll() {
        em.flush();
        em.clear();
        em.createQuery("DELETE FROM Student").executeUpdate();
    }


    @Override
    public void deleteById(String email) {
        em.createQuery("DELETE FROM Student s WHERE s.email = :id")
                .setParameter("id", email)
                .executeUpdate();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public List<Student> findAll() {
        return em.createQuery("SELECT s FROM Student s")
                .getResultList();
    }

    @Override
    public Iterable<Student> findAllById(Iterable<String> email) {
        return em.createQuery("SELECT s FROM Student s WHERE s.email in (:ids)")
                .setParameter("ids", email)
                .getResultList();
    }

    @Override
    public Optional<Student> findById(String s) {
        return Optional.empty();
    }

    @Override
    public <S extends Student> S save(S entity) {
        if (entity == null) {
            throw new IllegalArgumentException();
        }
        if (existsById(entity.getEmail())) {
            em.merge(entity);
        } else {
            em.persist(entity);
        }
        em.flush();
        Optional<Student> s = findById(entity.getEmail());
        return (S) s.orElse(null);
    }

}
