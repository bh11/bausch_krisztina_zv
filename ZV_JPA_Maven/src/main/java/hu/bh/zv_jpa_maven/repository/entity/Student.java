package hu.bh.zv_jpa_maven.repository.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Student {

    @Id
    private String email;
    private String firstName;
    private String lastName;

}
