package hu.bh.zv_jpa_maven.repository.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Abc {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String letters;
}
