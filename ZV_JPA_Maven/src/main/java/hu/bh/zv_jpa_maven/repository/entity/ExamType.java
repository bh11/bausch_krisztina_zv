package hu.bh.zv_jpa_maven.repository.entity;

import javax.persistence.Enumerated;

public enum ExamType {

    FIRSTEXAM,
    SECONDEXAM,
    FINALEXAM

}
