/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.bh.zv_jpa_maven.repository;

import java.util.List;
import java.util.Optional;

public interface CRUDRepository<T, ID> {


    void delete(T entity);
    void deleteAll();
    void deleteById(ID id);
    boolean existsById(ID id);
    List<T> findAll();
    Iterable<T> findAllById(Iterable<ID> ids);
    Optional<T> findById(ID id);
    <S extends T> S save(S entity);
}
