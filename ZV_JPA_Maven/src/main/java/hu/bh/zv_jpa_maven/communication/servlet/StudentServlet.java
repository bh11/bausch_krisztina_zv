package hu.bh.zv_jpa_maven.communication.servlet;

import hu.bh.zv_jpa_maven.repository.repositoryclasses.StudentDao;
import hu.bh.zv_jpa_maven.service.serviceclasses.StudentService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "StudentServlet", urlPatterns = {"/Students"})
public class StudentServlet extends HttpServlet {

    @Inject
    StudentService service;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doGet(req, resp);
        req.setAttribute("students", service.getAllStudents());
        req.getRequestDispatcher("Abc.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
