
package hu.bh.zv_jpa_maven.service.utilities;

public interface Mapper<E, D> {
    E mapToEntity(D dto);
    D mapToDTO(E entity);
}
