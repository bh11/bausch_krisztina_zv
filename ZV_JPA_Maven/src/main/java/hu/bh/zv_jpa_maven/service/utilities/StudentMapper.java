package hu.bh.zv_jpa_maven.service.utilities;

import hu.bh.zv_jpa_maven.repository.entity.Student;
import hu.bh.zv_jpa_maven.service.dto.StudentDto;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class StudentMapper implements Mapper<Student, StudentDto>{
    @Override
    public Student mapToEntity(StudentDto dto) {
        Student entity = new Student();
        entity.setEmail(dto.getEmail());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        return entity;
    }

    @Override
    public StudentDto mapToDTO(Student entity) {
        StudentDto dto = new StudentDto();
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        return dto;
    }
}
