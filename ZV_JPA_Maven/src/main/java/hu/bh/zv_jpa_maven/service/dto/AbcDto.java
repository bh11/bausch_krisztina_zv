package hu.bh.zv_jpa_maven.service.dto;

import lombok.Data;

@Data
public class AbcDto {

    private Integer id;
    private String letters;
}
