package hu.bh.zv_jpa_maven.service.serviceclasses;


import hu.bh.zv_jpa_maven.repository.entity.Student;
import hu.bh.zv_jpa_maven.repository.repositoryclasses.StudentDao;
import hu.bh.zv_jpa_maven.service.dto.StudentDto;
import hu.bh.zv_jpa_maven.service.utilities.StudentMapper;
import lombok.Setter;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
@Setter
public class StudentService {

    @Inject
    StudentDao dao;

    @Inject
    StudentMapper mapper;

    public List<StudentDto> getAllStudents () {
        List<StudentDto> result = new ArrayList<>();
        return dao.findAll()
                .stream()
                .map(mapper::mapToDTO)
                .collect(Collectors.toList());

    }
}
