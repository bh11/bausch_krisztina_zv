package hu.bh.zv_jpa_maven.service.dto;

import lombok.Data;

@Data
public class StudentDto {

    private String email;
    private String firstName;
    private String lastName;

}
