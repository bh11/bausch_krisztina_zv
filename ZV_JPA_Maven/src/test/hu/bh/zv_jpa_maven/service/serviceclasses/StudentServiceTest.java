package hu.bh.zv_jpa_maven.service.serviceclasses;

import hu.bh.zv_jpa_maven.repository.repositoryclasses.StudentDao;
import hu.bh.zv_jpa_maven.service.dto.StudentDto;
import hu.bh.zv_jpa_maven.service.utilities.StudentMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {

    @Mock
    StudentDao dao;

    @Mock
    StudentDto dto;

    @Mock
    StudentMapper mapper;

    @InjectMocks
    private final StudentService underTest = new StudentService();


    @Test
    void testGetAllStudentsWhenDaoReturnsEmptyList() {
        // Given
        Mockito.when(dao.findAll()).thenReturn(new ArrayList<>());
        // When
        List<StudentDto> studentDtos = underTest.getAllStudents();
        // Then
        assertEquals(new ArrayList<>(), studentDtos);
    }
}