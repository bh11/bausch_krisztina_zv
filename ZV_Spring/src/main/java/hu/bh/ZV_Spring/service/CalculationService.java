package hu.bh.ZV_Spring.service;

import hu.bh.ZV_Spring.repository.CalculationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculationService {


private  static  final  String PLUS = "+";


    @Autowired
    private CalculationRepository repo;


    public Integer setNumber(int first, int second, String operator) {
        int sum = 0;

        switch (operator.toString()) {
            case PLUS: sum = add(first, second); break;
            case "-": sum = sub(first, second); break;
            case "/": sum = divide(first, second); break;
            case "*": sum = multiply(first, second); break;
        }
        return sum;
    }

    public int add(int first, int second){
        return first + second;
    }
    public int sub(int first, int second){
        return first - second;
    }
    public int multiply(int first, int second){
        return first * second;
    }
    public int divide(int first, int second){
        return first / second;
    }
}
