package hu.bh.ZV_Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZvSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZvSpringApplication.class, args);
	}

}
