package hu.bh.ZV_Spring.repository.entity;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement
public class Calculation {

    private int firstNumber;
    private int secondNumber;
}
