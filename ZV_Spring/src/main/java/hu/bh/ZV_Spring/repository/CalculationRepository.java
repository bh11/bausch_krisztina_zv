package hu.bh.ZV_Spring.repository;

import hu.bh.ZV_Spring.repository.entity.Calculation;
import org.springframework.stereotype.Repository;

@Repository
public class CalculationRepository {

    private Calculation calculation;

    public CalculationRepository(Calculation calculation) {
        this.calculation = calculation;
    }

    public CalculationRepository() {
    }
}
