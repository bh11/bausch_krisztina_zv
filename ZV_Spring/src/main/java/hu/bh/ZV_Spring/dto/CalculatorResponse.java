package hu.bh.ZV_Spring.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CalculatorResponse {

    @JsonProperty("_finalNumber")
    private int finalNumber;

}
