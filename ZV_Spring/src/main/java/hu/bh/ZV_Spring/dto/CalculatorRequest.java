package hu.bh.ZV_Spring.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class CalculatorRequest implements Serializable {

    @JsonProperty("_firstNumber")
    private int firstNumber;

    @JsonProperty("_secondNumber")
    private int secondNumber;

    @JsonProperty("_operator")
    private String operator;

}
