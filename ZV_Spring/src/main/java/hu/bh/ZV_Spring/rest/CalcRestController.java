package hu.bh.ZV_Spring.rest;

import hu.bh.ZV_Spring.dto.CalculatorRequest;
import hu.bh.ZV_Spring.dto.CalculatorResponse;
import hu.bh.ZV_Spring.service.CalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/calculator")
public class CalcRestController {

    @Autowired
    CalculationService service;

    @RequestMapping(value = "/calculate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity filter(@RequestBody CalculatorRequest calc) {
        int sum = service.setNumber(calc.getFirstNumber(), calc.getSecondNumber(), calc.getOperator());

        CalculatorResponse resp = new CalculatorResponse();
        resp.setFinalNumber(sum);
        return ResponseEntity.ok(resp);
    }
}